var app = new Vue({
    el: '#app',

    data: {
        message: 'Hello Vue.js!',
        activedelay:false,
        yes: false,
        no: false,
        currentmsgchat:0,
        chisiamoactive:false,
        dovesiamoactive:false,
        margintopchat:true,
        hungryiconchecked:10, //i valori vanno da 0 a 4 quindi 10 è un numero random
        blogactive:false,
        contactactive:false,
        menu:true,
        hero_left:true,
        mobile:false,
        chattext: [
            {text:'Ciao! :-) Hai fame?'},
            {text:'Cosa vorresti mangiare?'},
            {text:'No? Forse riesco a farti venire fame con alcune foto dei nostri piatti!'},
            {text:'Vieni a trovarci! Ti aspetto :)'},
            {text:'Come preferisci contattarci?'},
            {text:'Ecco i nostri eventi, ricette e info. Buona lettura!'},
        ],
        welcomeiconmenu: [
            { img: '/img/icon/fish.png'},
            { img: '/img/icon/steak.png'},
            { img: '/img/icon/hamburger.png'},
            { img: '/img/icon/veg.png'}
        ],
        welcomeimgpiatti: [
            { img: '/img/foto/BUNE_spaghetti.jpg'},
            { img: '/img/foto/BUNE_melanzane.jpg'},
            { img: '/img/foto/BUNE_polpo.jpg'},
            { img: '/img/foto/BUNE_scoglio.jpg'}
        ]
    },
     // mounted:function(){
     //         //method1 will execute at pageload
     //        this.$nextTick(function() {
     //            window.addEventListener('resize', this.getWindowWidth);
     //              //Init
     //              this.getWindowWidth()
     //        }),

     //        this.activeritardo()
     //    },

    methods: {
        chisiamo: function() {
            this.chisiamoactive = true
            this.contactactive = false
            this.margintopchat = false
            this.blogactive = false
            this.dovesiamoactive = false
        },
        blog: function() {
            this.currentmsgchat = 5
            this.yes = false
            this.chisiamoactive = false
            this.contactactive = false
            this.margintopchat = false
            this.dovesiamoactive = false
            this.blogactive = true
        },
        dovesiamo: function() {
            this.currentmsgchat = 3
            this.yes = false
            this.margintopchat = false
            this.blogactive = false
            this.contactactive = false
            this.chisiamoactive = false
            this.dovesiamoactive = true
        },
        contattaci: function() {
            this.currentmsgchat = 4
            this.yes = false
            this.margintopchat = false
            this.blogactive = false
            this.chisiamoactive = false
            this.dovesiamoactive = false
            this.contactactive = true
        },
           
         activeritardo: function () {
            this.activedelay =! this.activedelay
        },
        timer: function() {
            //this.active =! this.active   
            setInterval(function(){ this.active =! this.active; }, 3000);
        },
         grab: function () {
            event.preventDefault()
          // var clickedElement = event.target
          // grab clicked id
          var clickedElement = event.currentTarget.id
          this.hungryiconchecked = clickedElement
          console.log(clickedElement)
          console.log(this.hungryiconchecked)
          this.dovesiamoactive = false
          this.chisiamoactive = false

        },
        imhungry: function() {
            //this.active =! this.active   
            this.yes =! this.yes
            console.log(this.yes)
            this.currentmsgchat = 1
            this.margintopchat = false
            console.log(this.currentmsgchat)
            this.dovesiamoactive = false
            this.chisiamoactive = false


        },
        imnothungry: function() {
            //this.active =! this.active   
            //this.no =! this.no
            //console.log(this.no)
            this.margintopchat = false
            this.currentmsgchat = 2
            console.log(this.currentmsgchat)
            this.dovesiamoactive = false
            this.chisiamoactive = false
        },
        clickiconhungry: function() {

        },
        
      //   getWindowWidth(event) {
      //   this.windowWidth = document.documentElement.clientWidth;
      //     if (this.windowWidth <= 479) {
      //       console.log("è minore di 479");
      //       this.margintopchat = false
      //       this.menu = false
      //       this.hero_left = false
      //       this.mobile = true
      //     }
      // },



    },

    // beforeDestroy() {
    //     window.removeEventListener('resize', this.getWindowWidth);
    // }

    });