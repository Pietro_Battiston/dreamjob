<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contacts', function ()
{
    return view('componenti.contattaci');

});

Route::post('/contacts/send', function(Request $request) {

	$data = $request->input();

	Mail::send('email.template', ['data' => $data], function ($m) {
			$to = 'info@dream-job.it';
			$toName = 'DreamJob';
            $m->from('info@dream-job.it', 'DreamJob');
            $m->to($to, $toName);
            $m->subject('Nuovo Contatto');
    });
	$request->session()->flash('alert-success', 'Messaggio inviato! Ti ricontatteremo al più presto.');
	return redirect('/')->with('success', 'contacts.ok');
});



Route::get('/sitemap', 'SitemapController@index');




Route::get('/chi-siamo', function()
{
    return view('chi-siamo');
});

Route::get('/contatti', function()
{
    return view('pagina-contatti');
});

Route::get('/privacy-policy', function()
{
    return view('privacy-policy');
});

Route::get('/partecipa', function()
{
    return view('partecipa');
});



Route::get('/', 'CategoriesController@index')->name('home');


Auth::routes();


// Route::get('/blog', 'ArticlesController@blog')->name('blogindex');
// Route::get('/corsi/{slug}', 'CategoriesController@category')->name('category');
// Route::get('/{aula_o_online}/{slug}', 'ArticlesController@article')->name('articolo');;





Route::group(['middleware' => 'admin'], function () {
	Route::get('/admin', 'ArticlesController@adminindex');
	Route::get('/admin/corsi', 'ArticlesController@admincorsi');
	Route::get('/admin/add-corso', 'ArticlesController@add');
	Route::post('/admin/add-corso', 'ArticlesController@save');
	Route::get('/admin/{id}/edit', 'ArticlesController@edit')->name('editarticle');
	Route::put('/admin/{id}/update', 'ArticlesController@update');
	Route::get('/admin/{id}/delete', 'ArticlesController@destroy');
	Route::get('/admin/categorie', 'CategoriesController@admincategorie');
	Route::get('/admin/categorie/{id}/edit', 'CategoriesController@edit');
	Route::put('/admin/categorie/{id}/update', 'CategoriesController@update');
	Route::get('/admin/categorie/{id}/delete', 'CategoriesController@destroy');
	Route::get('/admin/add', 'CategoriesController@add');
	Route::post('/admin/add', 'CategoriesController@save');



	Route::get('/admin/users', 'UserController@index')->name('useradminindex');
	Route::PUT('/admin/{id}/makeadmin', 'UserController@makeadmin');
	Route::get('/admin/{id}/makeadmin', 'UserController@makeadmin');

});

Route::get('corsi-in-aula/', 'CategoriesController@index')->name('categorie-in-aula');
Route::get('corsi-online/', 'CategoriesController@index')->name('categorie-online');


// Route::get('/{category?}', function($category = null)
//     {
//         // get all the blog stuff from database
//         // if a category was passed, use that
//         // if no category, get all posts
//         // if ($category)
//             $posts = Post::where('category', '=', $category);
//         // else
//             $posts = Post::all();

//         // show the view with blog posts (app/views/blog.blade.php)
//         return View::make('blog')
//             ->with('posts', $posts);
//     });

// Route::get('corsi-in-aula/corso/{slug}', 'ArticlesController@article')->name('articolo-in-aula');
// Route::get('corsi-online/corso/{slug}', 'ArticlesController@article')->name('articolo-online');


 // Route::get('corsi-in-aula/{slug}', 'CategoriesController@category')->name('categoria-in-aula');
 // Route::get('corsi-online/{slug}', 'CategoriesController@category')->name('categoria-online');




Route::get('corsi/{slug}', 'CategoriesController@category')->name('categoria');


Route::get('/{slug}', 'ArticlesController@article')->name('corso');




