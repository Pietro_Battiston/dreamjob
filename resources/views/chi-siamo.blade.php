@extends('layouts.app')
@section('title_and_meta')
        <title>DreamJob - Scuola di formazione professionale</title>
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!">
        <meta name="robots" content="index,follow">
        <meta property="og:title" content="DreamJob - Corsi professionali in Aula e Online"/>
        <meta property="og:site_name" content="DreamJob"/>
        <meta property="og:image" content="/img/home/homepage-immagine.png"/>
       
@endsection

@section('content')
       
<div class="container-fluid chi-siamo-fluid">
	<div class="col-md-12 immagine-chi-siamo">
		<img src="/img/chi-siamo-immagine.jpg" alt="dreamjob corsi professionali in aula e online" class="img-responsive">
	</div>
</div>
<div class="container chi-siamo">
	
	<h1>DreamJob <br> Corsi professionali in aula e online</h1>

	<h2>
		DreamJob è una nuova realtà professionale che offre a coloro che vogliono specializzarsi professionalmente la possibilità di <b>acquisire le competenze necessarie per entrare a far parte nel mondo del lavoro</b>. <br>
		Dreamjob organizza <b>corsi di formazione</b> privati, mettendo in contatto i potenziali professionisti <b>con esperti altamente qualificati</b>, organizzando <b>stage formativi</b> per immergerli sin da subito nella realtà lavorativa dei propri sogni. <br>
		Attraverso una formazione mirata, sia teorica che pratica, i nostri esperti saranno in grado di trasmettere le loro competenze ai professionisti del domani con <b>metodi innovativi, rapidi ed efficienti</b>. <br>
	</h2>
</div>
<div class="container-fluid chi-siamo-fluid">
	<div class="col-md-12 immagine-chi-siamo">
		<img src="/img/chi-siamo-immagine-2.jpg" alt="dreamjob corsi professionali in aula e online" class="img-responsive">
	</div>
</div>
<div class="container chi-siamo">
	<h2>
		Dreamjob offre <b>un’ampia gamma di corsi di formazione professionale</b> che spaziano dal Food&amp;Beverage al Digital, dall’Estetica al Design, coprendo ogni tipologia di esigenza lavorativa richiesta. <br>
		Tramite un’<b>assistenza costante durante l’intero processo di formazione</b>, i corsisti potranno contare sull’appoggio di tutor preposti all’insegnamento che li guideranno dalle fasi iniziali fino all’<b>attestazione finale delle competenze acquisite</b>, e, qualora necessario, potranno essere instradati verso la scelta della professione più adatta alle loro inclinazioni lavorative.
	</h2>
</div>
<div class="container-fluid chi-siamo-fluid">
	<div class="col-md-12 icone-footer">
                <div class="col-md-4 icona-footer">
                    <i class="fa fa-pencil-square-o fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        CORSI SIA IN AULA CHE ONLINE
                      </h3>
                    </div>
                </div>
                 <div class="col-md-4 icona-footer">
                    <i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        ATTESTATO DELLE COMPETENZE ACQUISITE
                      </h3>
                    </div>
                </div>
                 <div class="col-md-4 icona-footer">
                    <i class="fa fa-clock-o fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        SUPPORTO CONTINUATIVO
                      </h3>
                    </div>
                </div>
    </div>
</div>
 <div class="container iscrivitisubito">
           <a href="/"> <button><h3>SCOPRI ORA I NOSTRI CORSI!</h3></button></a>
 </div>



	





@endsection