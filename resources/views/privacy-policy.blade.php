@extends('layouts.app')
@section('title_and_meta')
        <title>Privacy Policy - DreamJob - Scuola di formazione professionale</title>
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!">
        <meta name="robots" content="noindex, follow">
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!"/>
        <meta property="og:title" content="DreamJob - Corsi professionali in Aula e Online"/>
       
        <meta property="og:site_name" content="DreamJob Corsi professionali in Aula e Online"/>
       
@endsection

@section('content')

       
<div class="container" style="margin-top:5em; margin-bottom:5em;">
	<div class="col-md-12">
				<h1 style="text-align: center;">INFORMATIVA SULLA PRIVACY</h1>

	<h2 style="font-size:1.2em;">
	Ai sensi dell’art. 13 del d.lgs.vo n.196/2003
	La scrivente Società informa che per l’instaurazione e l’esecuzione dei rapporti contrattuali
	con Voi in corso è in possesso di dati anagrafici e fiscali acquisiti anche verbalmente
	direttamente o tramite terzi, a Voi relativi, dati qualificati come personali dalla legge
	indicata in oggetto. <br>
	Con riferimento a tali dati Vi informiamo che: <br>
	– i dati vengono trattati in relazione alle esigenze contrattuali ed ai conseguenti
	adempimenti degli obblighi legali e contrattuali dalle stesse derivanti, per consentire una
	efficace gestione dei rapporti commerciali, nonché, per finalità statistiche, di marketing,
	promozionali, di controllo qualità e di tutela del credito. <br>
	I dati verranno trattati in forma scritta e/o su supporto magnetico, elettronico o telematico:
	– il conferimento dei dati stessi è obbligatorio per tutto quanto è richiesto dagli obblighi
	legali e contrattuali e pertanto l’eventuale rifiuto a fornirli o al successivo trattamento potrà
	determinare l’impossibilità della scrivente a dar corso ai rapporti contrattuali medesimi;<br>
	– il mancato conferimento, invece, di tutti i dati che non siano riconducibili ad obblighi
	legali o contrattuali verrà valutato dalla scrivente di volta in volta e determinerà le
	conseguenti decisioni rapportate all’importanza dei dati richiesti rispetto alla gestione del
	rapporto commerciale;<br>
	– ferme restando le comunicazioni e diffusioni effettuate in esecuzione di obblighi di legge
	i dati potranno essere comunicati in Italia e/o all’estero.<br>
	– i dati verranno trattati per tutta la durata del rapporto contrattuale instaurato e anche
	successivamente per l’espletamento di tutti gli adempimenti di legge nonché per future
	finalità commerciali;<br>
	– relativamente ai dati medesimi potete esercitare i diritti previsti all’art.7 del d.lgs.vo
	n.196/2003 (vedasi sotto) nei limiti ed alle condizioni previste daglii articoli 8, 9 e 10 del
	citato decreto legislativo; <br>
	– titolare del trattamento dei Vostri dati personali è Dreamjob di Angelo Serafino – via
	Zanardelli 94– Bari – P.IVA 08059390727 – tel+39 3511329869<br>

	– info@dream-job.it nella persona di Angelo Serafino. <br>
	Art. 7 del d.lgs.vo n.196/2003 <br>
	1. L’interessato ha diritto di ottenere la conferma dell’esistenza o meno di dati personali
	che lo riguardano, anche se non ancora registrati, e la loro comunicazione in forma
	intelligibile. <br>
	2. L’interessato ha diritto di ottenere l’indicazione: <br>
	a) dell’origine dei dati personali; <br>
	b) delle finalità e modalità del trattamento; <br>
	c) della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti
	elettronici; <br>
	d) degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai
	sensi dell’articolo 5, comma 2; <br>
	e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere
	comunicati o che possono venirne a conoscenza in qualità di rappresentante designato
	nel territorio dello Stato, di responsabili o incaricati. <br>

	3. L’interessato ha diritto di ottenere: <br>
	a) l’aggiornamento, la rettificazione ovvero, quando vi ha interesse, l’integrazione dei dati; <br>
	b) la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in
	violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione
	agli scopi per i quali i dati sono stati raccolti o successivamente trattati; <br>
	c) l’attestazione che le operazioni di cui alle lettere a) e b) sono state portate a
	conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati
	comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o
	comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato. <br>
	4. L’interessato ha diritto di opporsi, in tutto o in parte: <br>
	– per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti
	allo scopo della raccolta; <br>
	– al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o
	di vendita diretta o per il compimento di ricerche di mercato o di comunicazione
	commerciale <br>
	Art. 9 del d.lgs.vo n. 196/2003 <br>
	1. La richiesta rivolta al titolare o al responsabile può essere trasmessa anche mediante
	lettera raccomandata, telefax o posta elettronica. Il Garante può individuare altro idoneo
	sistema in riferimento a nuove soluzioni tecnologiche. Quando riguarda l’esercizio dei diritti
	di cui all’art. 7, commi 1 e 2, la richiesta può essere formulata anche oralmente e in tal
	caso è annotata sinteticamente a cura dell’incaricato o del responsabile . <br>
	2. Nell’esercizio dei diritti di cui all’articolo 7 l’interessato può conferire, per iscritto, delega
	o procura a persone fisiche, enti, associazioni od organismi. L’interessato può, altresì, farsi
	assistere da una persona di fiducia. <br>
	3. I diritti di cui all’articolo 7 riferiti a dati personali concernenti persone decedute possono
	essere esercitati da chi ha un interesse proprio, o agisce a tutela dell’interessato o per
	ragioni familiari meritevoli di protezione. <br>
	4. L’identità dell’interessato è verificata sulla base di idonei elementi di valutazione, anche
	mediante atti o documenti disponibili o esibizione o allegazione di copia di un documento
	di riconoscimento. La persona che agisce per conto dell’interessato esibisce o allega
	copia della procura, ovvero della delega sottoscritta in presenza di un incaricato o
	sottoscritta e presentata unitamente a copia fotostatica non autenticata di un documento di
	riconoscimento dell’interessato. Se l’interessato è un persona giuridica, un ente o
	un’associazione, la richiesta è avanzata dalla persona fisica legittimata in base ai rispettivi
	statuti od ordinamenti. <br>
	5. La richiesta di cui all’articolo 7, commi 1 e 2, è formulata liberamente e senza costrizioni
	e può essere rinnovata, salva l’esistenza di giustificati motivi, con intervallo non minore di
	novanta giorni. <br>
	</h2>
	</div>
</div>

@endsection