<div class="modal fade" id="modal_menuristorante" tabindex="-1" role="dialog" aria-labelledby="menu ristorante bune">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-md-10">
                <h4 class="modal-title" id="myModalLabel">IL NOSTRO MENU</h4>
            </div>
            <div class="col-md-2">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
          </div>
          <div class="modal-body">
            <img src="/img/menu/antipasti-bune.jpg" alt="antipasti" class="img-responsive">
            <img src="/img/menu/primi-piatti-bune.jpg" alt="primi" class="img-responsive">
            <img src="/img/menu/secondi-piatti-bune-pesce.jpg" alt="secondi di pesce" class="img-responsive">
            <img src="/img/menu/secondi-piatti-bune-carne.jpg" alt="secondi di carne" class="img-responsive">
            <img src="/img/menu/insalate-bune.jpg" alt="insalate" class="img-responsive">
            <img src="/img/menu/dolci-bune.jpg" alt="dolci" class="img-responsive">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">CHIUDI</button>
          </div>
        </div>
      </div>
</div>

<div class="modal fade" id="modal_comeraggiungerci" tabindex="-1" role="dialog" aria-labelledby="come raggiungerci">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-md-10">
                <h4 class="modal-title" id="myModalLabel">COME RAGGIUNGERCI</h4>
            </div>
            <div class="col-md-2">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
          </div>
          <div class="modal-body">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2703.733486937862!2d18.158945950652342!3d39.85998397933276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMznCsDUxJzM1LjkiTiAxOMKwMDknNDAuMSJF!5e1!3m2!1sit!2sit!4v1496598338460" width="400" height="300" frameborder="0" style="border:0" allowfullscreen class="iframemap"></iframe>
            <h3>Bune ristorante / bistrot si trova a Torre Mozza (Via Giovanni Battista Tiepolo), Marina di Ugento, il vero cuore del Salento.
                Oltre alla fantastica vista sul mare, da Bune è possibile accedere alla spiaggia libera, caratterizzata da sabbia grigia ed acqua cristallina.
            </h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">CHIUDI</button>
          </div>
        </div>
      </div>
</div>