@extends('layouts.admin')
@section('title_and_meta')
        <title>ADMIN PANEL</title>
        
        <meta name="robots" content="index,nofollow">
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=yaoqe9sskkgy5zje1x8q44ji31nwcymsngpc3ro2da99mqhr"></script>
@endsection

@section('content')
    <div class="container">
         <form method="POST" action="update" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label for="title">Titolo</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ $category->title}}">
                </div>

                <div class="form-group">
                    <label for="text">Testo</label>
                    <textarea name="text" class="form-control editortext" id="text">{{ $category->text }}</textarea>
                </div>

                <div class="form-group">
                    <label for="immagine">IMMAGINE</label>
                    <input type="file" name="img">
                    <img src="/img/{{$category->img}}" alt="" class="img-responsive">
                </div>
                <div class="form-group">
                    <label for="alt_img">Descrizione immagine</label>
                    <input type="text" name="alt_img" class="form-control" id="alt_img" value="{{ $category->alt_img }}">
                </div>



                <div class="form-group">
                    <label for="meta title">Meta title</label>
                    <input type="text" name="metatitle" class="form-control" id="metatitle" value="{{ $category->metatitle }}">
                </div>

                <div class="form-group">
                    <label for="meta desc">Meta desc</label>
                    <input type="text" name="metadesc" class="form-control" id="metadesc" value="{{ $category->metadesc }}">
                </div>

                <button type="submit" class="btn btn-default">Add</button>
        </form>
    </div>




<script>tinymce.init({ selector:'textarea.editortext' });</script>

@endsection
