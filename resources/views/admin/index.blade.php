@extends('layouts.admin')



@section('content')
    <div class="container-fluid" style="background-color:#073558; height: 100vh; ">

        <div class="col-md-6" style="text-align: center; margin-top:12em;">
            <a href="/admin/corsi" style="color:white">
                <i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
                <div class="col-md-12" style="margin-top:1em; font-weight: bold;">
                    CORSI
                </div>
            </a>
         </div>
         <div class="col-md-6" style="text-align: center; margin-top:12em; color:white">
            <a href="/admin/categorie" style="color:white">
                 <i class="fa fa-object-group fa-4x" aria-hidden="true"></i>
                 <div class="col-md-12" style="margin-top:1em; font-weight: bold;">
                     CATEGORIE
                 </div>
             </a>
         </div>
         

        <div class="col-md-12">
            <h1 style="color:white; font-family: 'Lato', sans-serif !important; font-size:30px; letter-spacing:2px; text-align: center; position:fixed; bottom:1em;">
                {!!$frase_random!!}
            </h1>


        </div>
         
         

    </div>


  
<script>tinymce.init({ selector:'textarea' });</script>

@endsection
