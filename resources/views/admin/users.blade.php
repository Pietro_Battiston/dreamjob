@extends('layouts.admin')



@section('content')
    
         <div class="container">
                
                
                <div class="col-md-12" style="border-bottom:1px solid black; margin-bottom:1em;">
                    <h3>UTENTI</h3>
                </div>
            @foreach ($users as $user)
                <div class="col-md-8">
                    <table>
                        <tr>
                            <th style="padding-right:5em; padding-left:5em;">Username</th>
                            <th style="padding-right:5em; padding-left:5em;">Email</th>
                            <th style="padding-right:5em; padding-left:5em;">Ruolo</th>
                        </tr>
                        <tr>
                            <td style="padding-right:5em; padding-left:5em; padding-top:1em;">{{$user->name}}</td>
                            <td style="padding-right:5em; padding-left:5em; padding-top:1em;">{{$user->email}}</td>
                            <td style="padding-right:5em; padding-left:5em; padding-top:1em;"> 
                                @if ($user->is_admin == 1)
                                     ADMIN
                                @endif
                            </td>
                        </tr>
                    </table>
                       

                </div>
                <div class="col-md-4" style="padding-top:2em;">
                    <form action="/admin/{{$user->id}}/makeadmin" method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if ($user->is_admin != 1)
                        <a class="btn btn-small btn-danger" href="/admin/{{$user->id}}/makeadmin">ADMIN</a>
                        @endif
                    </form>
                    <a class="btn btn-small btn-danger" href="/admin/{{$user->id}}/delete">CANCELLA</a>
                </div>
                
            @endforeach
        </div>
   





@endsection
