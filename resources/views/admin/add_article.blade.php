@extends('layouts.admin')

@section('title_and_meta')
        <title>ADMIN PANEL</title>
        
        <meta name="robots" content="index,nofollow">
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=yaoqe9sskkgy5zje1x8q44ji31nwcymsngpc3ro2da99mqhr"></script>
       
@endsection

@section('content')
    <div class="container-fluid" style="margin-top:5em;">
         <form method="POST" action="add-corso" enctype="multipart/form-data">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">


            <div class="form-group">
                <label for="title" style="color:#003054;">Titolo</label>
                <input type="text" name="title" class="form-control" id="title" value="{{ $notizia->title}}" required>
            </div>

            <div class="form-group">
                <label for="text" style="color:#003054;">Testo</label>
                <textarea name="text" class="form-control editortext" id="textarea">{{ $notizia->text }}</textarea>
            </div>

            <div class="form-group">
                <label for="corpo" style="color:#003054;">Immagine</label>
                <input type="file" name="img" required>
                <img src="{{ $notizia->img }}" alt="" class="img-responsive">
            </div>


            <div class="form-group">
                <label for="alt_img" style="color:#003054;">Descrizione immagine</label>
                <input type="text" name="alt_img" class="form-control" id="alt_img" value="{{ $notizia->alt_img }}" required>
            </div>


            <div class="form-group">
                <label for="meta title" style="color:#003054;">Meta title (se vuoi non utilizzarlo, lo genera automaticamente la piattaforma. puoi scrivere lettere a caso ma non lasciarlo vuoto altrimenti va in errore!)</label>
                <input type="text" name="metatitle" class="form-control" id="metatitle" value="{{ $notizia->metatitle }}" required>
            </div>

            <div class="form-group">
                <label for="meta desc" style="color:#003054;">Meta desc</label>
                <input type="text" name="metadesc" class="form-control" id="metadesc" value="{{ $notizia->metadesc }}" required>
            </div>

            <div class="form-group">
                <label for="category" style="color:#003054;">TIPOLOGIA</label>
                
                <select name = "tipologia">
                        <option value = "1" selected>IN AULA</option>
                        <option value = "2" selected>ONLINE</option>

                </select>
            </div>

            <div class="form-group">
                <label for="category" style="color:#003054;">CATEGORIA</label>
                
                <select name = "category">
                    @foreach ($categories as $category)
                        <option value = "{{ $category->id }}" selected>{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-default" style="background-color: #003054; color:white;">OK!</button>
        </form>
    </div>




<script>tinymce.init({
  selector: '#textarea',
  height: 500,
  plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
  // imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
});</script>

@endsection
