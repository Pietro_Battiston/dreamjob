@extends('layouts.admin')

@section('title_and_meta')
        <title>ADMIN PANEL</title>
        
        <meta name="robots" content="index,nofollow">
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=yaoqe9sskkgy5zje1x8q44ji31nwcymsngpc3ro2da99mqhr"></script>
       
@endsection

@section('content')
    <div class="container-fluid" style="margin-top:5em;">
        <form method="POST" action="add" enctype="multipart/form-data">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">


            <div class="form-group">
                <label for="title">Titolo</label>
                <input type="text" name="title" class="form-control" id="title" value="{{ $categories->title}}" required>
            </div>

            <div class="form-group">
                <label for="text">Testo</label>
                <textarea name="text" class="form-control editortext" id="textarea">{{ $categories->text }}</textarea>
            </div>

            <div class="form-group">
                <label for="corpo"></label>
                <input type="file" name="img" required>
                <img src="{{ $categories->img }}" alt="" class="img-responsive">
            </div>


            <div class="form-group">
                <label for="alt_img">Descrizione immagine</label>
                <input type="text" name="alt_img" class="form-control" id="alt_img" value="{{ $categories->alt_img }}" required>
            </div>


            <div class="form-group">
                <label for="meta title">Meta title (se vuoi non utilizzarlo, lo genera automaticamente la piattaforma. puoi scrivere lettere a caso ma non lasciarlo vuoto altrimenti va in errore!)</label>
                <input type="text" name="metatitle" class="form-control" id="metatitle" value="{{ $categories->metatitle }}" required>
            </div>

            <div class="form-group">
                <label for="meta desc">Meta desc</label>
                <input type="text" name="metadesc" class="form-control" id="metadesc" value="{{ $categories->metadesc }}" required>
            </div>

            <button type="submit" class="btn btn-default" style="background-color: #003054; color:white;">OK!</button>
        </form>
    </div>




@endsection
