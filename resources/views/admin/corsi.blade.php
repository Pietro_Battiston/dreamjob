@extends('layouts.admin')



@section('content')
    <div class="container">
                
                
                <div class="col-md-10" style="border-bottom:1px solid black; margin-bottom:1em;">
                    <h3>CORSI</h3>
                </div>
                <div class="col-md-2" style="margin-top:1em; position: fixed; right:15px; bottom:15px; text-align: right;">
                    <a href="/admin/add-corso">
                        <i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i>
                    </a>
                </div>
                    <div class="col-md-8">
                        <table>
                            <tr>
                                <th style="padding-right:5em; padding-left:5em;">Nome corso</th>
                                <th style="padding-right:5em; padding-left:5em;">Categoria</th>
                                <th style="padding-right:5em; padding-left:5em;">Azioni</th>
                            </tr>
                     @foreach ($corsi as $corso)
                     

                     
                            <tr>
                                <td style="padding-right:5em; padding-left:5em; padding-top:1em;">{{$corso->title}}</td>
                                <td style="padding-right:5em; padding-left:5em; padding-top:1em;">
                                    @foreach ($categorie as $categoria)
                                        @if ($corso->category_id == $categoria->id)
                                            {{$categoria->title}}
                                        @endif
                                    @endforeach

                                </td>
                                <td style="padding-right:5em; padding-left:5em; padding-top:1em;">
                                    <a href="/admin/{{$corso->id}}/edit">
                                        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                    </a>
                                    <a href="/admin/{{$corso->id}}/delete" style="margin-left:1em;">
                                        <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        
                           
                    @endforeach
                    </div>
                    
                   
                


                        </table>
    </div>
   

@endsection
