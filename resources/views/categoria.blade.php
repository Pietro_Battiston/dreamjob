@extends('layouts.app')
@section('title_and_meta')
        <title>{{$category->title}} - DreamJob</title>
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!">
        <meta name="robots" content="index,follow">
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!"/>
        <meta property="og:title" content="Corsi di formazione professionale {{ $category->title }} - DreamJob"/>
        <meta property="og:image" content="/img/{{$category->img}}"/>
        <meta property="og:site_name" content="DreamJob"/>
@endsection



@section('content')
    <div class="container-fluid pagina-categ-container" style="margin-top:5em">
        <div class="col-md-12 immagine-categoria-pagina">
                <img src="/img/{!!$category->img!!}" alt="" class="img-responsive">
        </div>
    </div>

    <div class="col-md-12 titolo-categ-pagina">
        <h1>SCEGLI IL LAVORO DEI TUOI SOGNI <br> TRA I CORSI {{$category->title}}</h1>
        <hr>
    </div>

        <div class="container" style="margin-bottom:5em">
            <div class="col-md-12">
            @foreach ($corsi_categ as $corso)
                    <div class="col-md-4 corso-singolo text-center">
                        <a href="{{route('corso', $corso->slug)}}">
                            <img src="/img/{{$corso->img}}" alt="{{$corso->alt_img}}" class="img-responsive">
                            <h2>{{$corso->title}}</h2>
                        </a>
                        <div class="button-su-categ pagina-categ-button-div">
                            <button><a href="{{route('corso', $corso->slug)}}">SCOPRI ORA</a></button>
                        </div>
                            
                    </div>
                
            @endforeach
            </div>

        </div>
    

@endsection
