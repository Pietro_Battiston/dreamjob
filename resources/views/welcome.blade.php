@extends('layouts.app')
@section('title_and_meta')
        <title>DreamJob - Scuola di formazione professionale</title>
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!">
        <meta name="robots" content="index,follow">
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!"/>
        <meta property="og:title" content="BUNE Ristorante - Corsi professionali in Aula e Online"/>
        <meta property="og:image" content="/img/logo-bune-rsz.png"/>
        <meta property="og:site_name" content="Bune Ristorante Bistrot a Torre Mozza"/>
       
@endsection

@section('content')
       
<div class="container-fluid homepage-container">
	<div class="col-md-12" id="immagine-grande-home">
		<img src="/img/home/homepage-immagine.png" alt="" class="img-responsive">
		<div class="testo-su-immagine col-md-12">
			<div class="col-md-12">
				<button><h2>SCOPRI I CORSI</h2></button>
			</div>
			<!-- <div class="col-md-6" id="secondobottonehome">
				<button><h2>CORSI ONLINE</h2></button>
			</div> -->
		</div>
		<!-- <div class="col-md-12 pulsanti-su-immagine text-center">
			<button id="primobottone">CORSI IN AULA</button>
			<button id="secondobottone">CORSI ONLINE</button>
		</div> -->
		<!-- <div class="col-md-6 pulsanti-su-immagine" id="primaicona">
			<i class="fa fa-book" aria-hidden="true"></i>
			<h3>Corsi in Aula</h3>
		</div>
		<div class="col-md-6 pulsanti-su-immagine" id="secondaicona">
			<i class="fa fa-desktop" aria-hidden="true"></i>
			<h3>Corsi Online</h3>

		</div> -->
	</div>

</div>

<div class="container-fluid" id="icone-home">
	<div class="col-md-4">
		<i class="fa fa-check-square fa-4x" aria-hidden="true"></i>
	<div class="col-md-12">
		<h3>SCEGLI IL CORSO CHE FA PER TE</h3>
	</div>
	</div>
	<div class="col-md-4">
		<i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
		<div class="col-md-12">
			<h3>APPRENDI NUOVE SKILL</h3>
		</div>
	</div>
	<div class="col-md-4">
		<i class="fa fa-briefcase fa-4x" aria-hidden="true"></i>
		<div class="col-md-12">
			<h3>LANCIATI NEL MONDO DEL LAVORO</h3>
		</div>
	</div>
</div>

<div class="container-fluid corsi-home">
	<div class="col-md-12" id="inizio-sezione-corsi-home">
		<h2>Corsi professionali in aula e online</h2>

	</div>
		@foreach ($categories as $category)
				<div class="col-md-4 corso-singolo text-center">
					<img src="/img/{{$category->img}}" alt="{{$category->alt_img}}" class="img-responsive">
					<div class="scritta-su-categ col-md-12">
						<h2>{{$category->title}}</h2>
					</div>
					<div class="col-md-12 button-su-categ">
						<button>SCOPRI ORA</button>
					</div>
				</div>
		@endforeach			
</div>


	





@endsection