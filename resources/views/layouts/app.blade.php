<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('title_and_meta')


    


    <!-- Styles -->

    
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        
        <link rel="shortcut icon" href="/img/sito/logo/dreamjob-ico.ico" type="image/x-icon">
        <link rel="icon" href="/img/sito/logo/dreamjob-ico.ico" type="image/x-icon">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        

        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500" rel="stylesheet">

        <link href="{{ asset('css/index2.css') }}" rel="stylesheet">
   

</head>

<body>
    <div id="app">
        @include('componenti.menu')  


        @yield('content')


        @include('componenti.contattaci')

        @include('componenti.footer')
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->

    <!-- Global Site Tag (gtag.js) - Google Analytics -->

    <script src="https://cdn.jsdelivr.net/npm/ga-lite@1/dist/ga-lite.min.js" async></script>
    <script>
    var galite = galite || {};
    galite.UA = 'UA-106967405-1'; // Insert your tracking code here
    </script>




   








 

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/f2707460fb.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#003054"
    },
    "button": {
      "background": "transparent",
      "border": "#ffffff",
      "text": "#ffffff"
    }
  },
  "content": {
    "message": "Questo sito utilizza cookies e tecnologie di terze parti per garantirti una navigazione migliore e raccogliere dati statistici. Continuando la navigazione e chiudendo questo messaggio dichiari di accettare.",
    "dismiss": "Ok!",
    "deny": "Rifiuto",
    "link": "Dettagli",
    "href": "https://www.iubenda.com/privacy-policy/8225102"
  }
})});
</script>
   

</body>

</html>
