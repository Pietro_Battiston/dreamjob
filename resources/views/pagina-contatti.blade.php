@extends('layouts.app')
@section('title_and_meta')
        <title> Contatti - DreamJob Scuola di formazione professionale</title>
        <meta name="robots" content="index,follow">
        <meta name="description" content="Contattaci subito e richiedi informazioni su i nostri corsi professionali"/>
        <meta property="og:title" content=" Contatti - DreamJob"/>
        <meta property="og:image" content="/img/home/homepage-immagine.png"/>
        <meta property="og:site_name" content="DreamJob"/>
        <meta property="og:description" content="Contattaci subito e richiedi informazioni su i nostri corsi professionali"/>

@endsection



@section('content')
           <div class="container" style="margin-top:6em; margin-bottom: 6em;">
                <div class="col-md-12" style="color:#003054; text-align: center; margin-bottom:2em;">
                    <h1>Come preferisci contattarci?</h1>
                </div>
                <div class="col-md-6">
                    <img src="/altreimmagini/contatti/call-center-dreamjob.png" alt="telefono" class="img-responsive" style="max-width: 525px;">
                    <div class="col-md-12" style="color:#003054; text-align: center;">
                        <h2>Telefono</h2>
                        <i class="fa fa-mobile fa-4x" aria-hidden="true"></i>
                        <p style="margin-top:1em;"><a href="tel:+393511329869" style="font-weight: bold; letter-spacing: 1px; text-decoration: underline;">351 132 9869</a></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="/altreimmagini/contatti/email-dreamjob.jpg" alt="email" class="img-responsive" style="max-width: 525px;">
                    <div class="col-md-12" style="text-align: center; color:#003054;">
                        <h2>E-mail</h2>
                        <i class="fa fa-at fa-4x" aria-hidden="true"></i>
                        <p style="margin-top:1em;"><a href="mailto:info@dream-job.it" style="font-weight: bold; text-decoration: underline;">info@dream-job.it</a></p>
                    </div>
                </div>

           </div>
@endsection
