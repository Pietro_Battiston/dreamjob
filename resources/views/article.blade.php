@extends('layouts.app')
@section('title_and_meta')
        <title>{{$article->title}} - DreamJob</title>
        <meta name="robots" content="index,follow">
        <meta name="description" content="{{$article->metadesc}}"/>
        <meta property="og:title" content="{{$article->title}} - DreamJob"/>
        <meta property="og:image" content="/img/{{$article->img}}"/>
        <meta property="og:site_name" content="DreamJob"/>
        <meta property="og:description" content="{{$article->metadesc}}"/>

@endsection



@section('content')
    <div class="container-fluid pagina-categ-container" style="margin-top:5em">
        <div class="col-md-12 immagine-categoria-pagina">
                <img src="/img/{{$article->img}}" alt="" class="img-responsive">
        </div>
         <div class="scopri-il-corso-hero col-md-12">
                <a href="#corso">
                    <h2>SCOPRI IL {{$article->title}}</h2>
                    <div class="col-md-12">
                        <i class="fa fa-arrow-down fa-3x" aria-hidden="true"></i>
                   </div>
                </a>
        </div>
    </div>

    <div class="col-md-12 titolo-categ-pagina" id="corso">
        <h1>{{$article->title}}</h1>
        <hr>
    </div>

        <div class="container-fluid" style="margin-bottom:5em">
            <div class="col-md-12 descrizione-corso">
                {!!$article->text!!}
            </div>

        </div>



        <div class="col-md-12 icone-footer">
                <div class="col-md-4 icona-footer">
                    <i class="fa fa-pencil-square-o fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        CORSO SIA IN AULA CHE ONLINE
                      </h3>
                    </div>
                </div>
                 <div class="col-md-4 icona-footer">
                    <i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        ATTESTATO DELLE COMPETENZE ACQUISITE
                      </h3>
                    </div>
                </div>
                 <div class="col-md-4 icona-footer">
                    <i class="fa fa-clock-o fa-4x" aria-hidden="true"></i>
                    <div class="col-md-12 icona-testo-footer">
                      <h3>
                        SUPPORTO CONTINUATIVO
                      </h3>
                    </div>
                </div>
        </div>
        <div class="container iscrivitisubito">
            <button data-toggle="modal" data-target="#largeModal"><h3>RICHIEDI INFORMAZIONI SUL {{$article->title}} !</h3></button>
        </div>

@endsection
