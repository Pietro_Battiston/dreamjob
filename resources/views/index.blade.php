@extends('layouts.app')
@section('title_and_meta')
        <title>BUNE Ristorante / Bistrot a Torre mozza, in Salento</title>
        <meta name="description" content="BUNE, ristorante / bistrot vista mare a Torre Mozza con accesso diretto alla spiaggia libera. Cucina pugliese, ingredienti a Chilometro 0, mare.">
        <meta name="robots" content="index,follow">
        <meta name="description" content="BUNE, ristorante / bistrot vista mare a Torre Mozza con accesso diretto alla spiaggia libera. Cucina pugliese, ingredienti a Chilometro 0, mare."/>
        <meta property="og:title" content="BUNE Ristorante / Bistrot a Torre mozza, in Salento"/>
        <meta property="og:image" content="/img/logo-bune-rsz.png"/>
        <meta property="og:site_name" content="Bune Ristorante Bistrot a Torre Mozza"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css">
@endsection

@section('content')
@include('modal')
<div class="container-fluid">
        <div class="row" id="bunewelcome">
            <div class="col-md-12 col-xs-12 no-padding">
                <img src="img/logo-bune.png" alt="benvenuti" class="img-responsive">
                <p>Ristorante / Bistrot fronte mare.</p>
            </div>
           <!--  <div class="col-md-4 col-xs-12 iconewelcome animated fadeInUp">
                <img src="img/icon/spaghetti.ico" alt="icona cibo" class="img-responsive iconaservice">
                <a href="#fotofood-drink-fun" class="iconaservice"><h3>FOOD</h3></a>
            
            </div>
            <div class="col-md-4 col-xs-12 iconewelcome animated fadeInUp">
                <img src="img/icon/birra.png" alt="icona drink" class="img-responsive iconaservice">
                <a href="#fotofood-drink-fun" class="iconaservice"><h3>DRINK</h3></a>
            </div>
            <div class="col-md-4 col-xs-12 iconewelcome animated fadeInUp">
                <img src="img/icon/fun.png" alt="icona mare" class="img-responsive iconaservice">
                <a href="#fotofood-drink-fun" class="iconaservice"><h3>FUN</h3></a>
            </div> -->
        </div>

           
        </div>
    </div>

   
     <!-- fine container fluid  -->

<div class="container-fluid">
    <!-- <div class="col-md-12"></div>
        <h3 class="scoprimenu">SCOPRI IL NOSTRO MENU</h3>
    </div> -->

    <div class="col-md-12 hidden-xs" id="testovideo">
                <h1>Ristorante / Bistrot fronte mare a Torre Mozza, Marina di Ugento</h1>
                <h2>Guardare il mare gustando un piatto della <b>tradizione culinaria Pugliese</b> rivisitato in chiave moderna, sorseggiando del buon vino.<br>Questo è <b>Bune</b>, ristorante / bistrot situato a Torre Mozza (Marina di Ugento) nel cuore del Salento.<br>
                Gli ingredienti dei nostri piatti sono a <b>Chilometro Zero</b> perchè diffondere le tradizioni e rispettare il territorio sono i nostri obiettivi.
                </h2>
               
    </div>
    

    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image:url(img/foto/spaghetti.jpg)"></div>
            <div class="swiper-slide" style="background-image:url(img/foto/insalata.jpg)"></div>
            <div class="swiper-slide" style="background-image:url(img/foto/sandwich.jpg)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/4)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/5)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/6)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/7)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/8)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/9)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/600/600/nature/10)"></div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>
    <!-- <div class="container">
        <div class="row" id="video_e_descrizione">
            <div class="col-md-6" id="testovideo">
                <h2>Guardare il mare gustando un piatto della <b>tradizione culinaria Pugliese</b> rivisitato in chiave moderna, sorseggiando del buon vino.<br>Questo è <b>Bune</b>, ristorante / bistrot situato a Torre Mozza nel cuore del Salento.<br>
                Gli ingredienti dei nostri piatti sono a <b>Chilometro Zero</b> perchè diffondere le tradizioni e rispettare il territorio sono i nostri obiettivi.
                </h2>
            </div>
             <div class="col-md-6">
                <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fbunebistrot%2Fvideos%2F234014670418685%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div> 
        </div>
    </div> -->
    <div class="col-md-3 hidden-xs">
    
    </div>


    <a href="" data-toggle="modal" data-target="#modal_menuristorante">
        <div class="col-md-6 col-sm-12 col-xs-12 text-center" id="intestazione_menu">
            
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">

                <div class="col-md-12">
                    <a href="" data-toggle="modal" data-target="#modal_menuristorante">
                        <h3 class="titolomenu">SCOPRI IL NOSTRO MENU</h3>
                    </a>
                </div>
                <div class="col-md-3">
                    <img src="img/icon/fish.png" alt="pesce" class="img-responsive iconemenu">
                </div>
                <div class="col-md-3">
                    <img src="img/icon/steak.png" alt="carne" class="img-responsive iconemenu">
                </div>
                <div class="col-md-3">
                    <img src="img/icon/veg.png" alt="vegetariano" class="img-responsive iconemenu">
                </div>
                <div class="col-md-3">
                    <img src="img/icon/hamburger.png" alt="panini" class="img-responsive iconemenu">
                </div>
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">
                <img src="img/sito/bunepallini.png" alt="sfondo pallini bune" class="img-responsive">

            
        </div>
    </a>

    <div class="col-md-3 hidden-xs">
    
    </div>


    
    
    <!-- <div class="col-md-4">
        <div class="col-md-12">
        </div>
        <div class="col-md-12">
            <button class="primobutton buttoninfo" data-toggle="modal" data-target="#modal_comeraggiungerci">DOVE SIAMO</button>
        </div>
        <div class="col-md-12">
            <button class="buttoninfo">CONTATTACI</button>
        </div>
        <div class="col-md-12">
            <button class="buttoninfo">SOCIAL NETWORK</button>
        </div>

    </div> -->

    <div class="col-md-12 hidden-xs" id="testovideo">
                <div class="col-md-4 text-center">
                    <button class="primobutton buttoninfo" data-toggle="modal" data-target="#modal_comeraggiungerci">DOVE SIAMO</button>
                </div>
                <div class="col-md-4 text-center">
                    <button class="buttoninfo primobutton">CONTATTACI</button>
                </div>
                <div class="col-md-4 text-center">
                    <button class="buttoninfo primobutton">SOCIAL NETWORK</button>
                </div>
    </div>

    
    
        <div class="col-md-12 text-center blogtitleindex hidden-xs">
            <a href="/blog"><h3>BLOG</h3></a>
        </div>
        
            @foreach ($news as $notizia)
                <div class="col-md-4 col-xs-12 articoliblogindex text-center">
                    <a href="/blog/{{$notizia->slug}}">
                        <img src="/img/{{$notizia->img}}" alt="{{$notizia->alt_img}}" class="img-responsive imgarticle">
                        <div class="titleblogindex">
                            <a href="/blog/{{$notizia->slug}}" class="titleblog-content">
                                {{$notizia->title}}
                            </a>
                        </div>
                    </a>
                </div>
            @endforeach
        









</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        effect: 'coverflow',
        grabCursor: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows : true
        }
    });
    </script>
@endsection
