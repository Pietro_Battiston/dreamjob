@extends('layouts.app')
@section('title_and_meta')
        <title>DreamJob - Scuola di formazione professionale</title>
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!">
        <meta name="robots" content="index,follow">
        <meta name="description" content="Scopri i nostri corsi professionali e lanciati nel mondo del lavoro!"/>
        <meta property="og:title" content="DreamJob - Corsi professionali in Aula e Online"/>
        <meta property="og:image" content="/img/home/homepage-immagine.png"/>
        <meta property="og:site_name" content="DreamJob - Corsi professionali in Aula e Online"/>
       
@endsection

@section('content')

<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
       
<div class="container-fluid homepage-container">
	<div class="col-md-12" id="immagine-grande-home">
		<img src="/img/home/homepage-immagine.png" alt="" class="img-responsive">
		<div class="testo-su-immagine col-md-12">
			<div class="col-md-12">
				<a href="#icone-home">
					<button><h2>SCOPRI I CORSI</h2></button>
				</a>
			</div>
		
		</div>
	</div>

</div>

<div class="container-fluid" id="icone-home">
	<div class="col-md-4 icona-home">
		<i class="fa fa-check-square fa-4x" aria-hidden="true"></i>
	<div class="col-md-12">
		<h3>SCEGLI IL LAVORO DEI TUOI SOGNI</h3>
	</div>
	</div>
	<div class="col-md-4 icona-home">
		<i class="fa fa-graduation-cap fa-4x" aria-hidden="true"></i>
		<div class="col-md-12">
			<h3>APPRENDI NUOVE SKILLS</h3>
		</div>
	</div>
	<div class="col-md-4 icona-home">
		<i class="fa fa-briefcase fa-4x" aria-hidden="true"></i>
		<div class="col-md-12">
			<h3>LANCIATI NEL MONDO DEL LAVORO</h3>
		</div>
	</div>
</div>

<div class="container-fluid corsi-home">
	<div class="col-md-12" id="inizio-sezione-corsi-home">
		<h2>Corsi professionali in aula e online</h2>

	</div>
		@foreach ($categories as $category)
				<div class="col-md-4 corso-singolo text-center">
					<a href="{{route('categoria', $category->slug)}}">
							<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="1500">
							   
							    <div class="carousel-inner">
							        <div class="item active">
							           <img src="/img/{{$category->img}}" alt="{{$category->alt_img}}" class="img-responsive">
							        </div>
								
							        @foreach ($corsi as $corso)
							        	
									        @if ($corso->category_id == $category->id)
									        <div class="item">
									            <img src="/img/{{$corso->img}}" alt="{{$category->alt_img}}">
									        </div>
									        @endif
									    

							        @endforeach
							  
							       
							    </div>
						
							</div>
					</a>
					<div class="scritta-su-categ col-md-12">
						<a href="{{route('categoria', $category->slug)}}">
							<h2>CORSI {{$category->title}}</h2>
						</a>
					</div>
					<div class="col-md-12 button-su-categ">
						<a href="{{route('categoria', $category->slug)}}">
							<button>SCOPRI I CORSI</button>
						</a>
					</div>
				</div>
		@endforeach		
	

		
</div>




@endsection