@extends('layouts.app')
@section('title_and_meta')
        <title> Richiesta partecipazione gratuita presentazione corsi - DreamJob</title>
        <meta name="robots" content="index,follow">
        <meta name="description" content="Iscriviti gratuitamente alla presentazione dei corsi"/>
        <meta property="og:title" content=" Richiesta partecipazione gratuita presentazione corsi - DreamJob"/>
        <meta property="og:image" content="/img/home/homepage-immagine.png"/>
        <meta property="og:site_name" content="DreamJob"/>
        <meta property="og:description" content="Richiesta partecipazione gratuita presentazione corsi"/>

@endsection



@section('content')
            @if(Session::has('success') && Session::get('success') == 'contacts.ok')
                          <div class="panel panel-success"> 
                                <div class="panel-heading"> 
                                  <h2 class="panel-title">Email inviata con successo!</h2> 
                                </div> 
                                <div class="panel-body"> 
                                  
                                  Abbiamo ricevuto il tuo messaggio, verrai ricontattato al più presto!

                                </div>
                          </div>
                        @else

                        <div class="col-md-12" style=" background-color:#003054; margin-top:6em; margin-bottom:3em;">
                         
                            <div class="col-md-offset-3 col-md-6">
                                <div class="form-area"> 
                                <!-- <h2 style="margin-bottom: 25px; text-align: center;">
                                  Contact Form
                                </h2> -->

                                    <form method="POST" action="/contacts/send" class="well span8">
                                  {{ csrf_field() }}
                                    <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Il tuo nome" required oninvalid="this.setCustomValidity('Per favore compila tutti i campi')">
                                  </div>
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="La tua Email" required oninvalid="this.setCustomValidity('Per favore compila tutti i campi')">
                                  </div>
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Il tuo numero di telefono">
                                  </div>
                                  
                                            <div class="form-group">
                                              <textarea class="form-control" type="textarea" name="message" id="message" placeholder="A quale presentazione di corso vorresti partecipare gratuitamente? (es: Corso Web Design)" maxlength="100" rows="2" required oninvalid="this.setCustomValidity('Per favore compila tutti i campi')"></textarea>
                                            </div>
                                            <div class="form-group">
                                              <textarea class="form-control" type="textarea" name="esperienza" id="esperienza" placeholder="Hai già avuto esperienze nel settore? (SI / NO)" maxlength="100" rows="1" required oninvalid="this.setCustomValidity('Per favore compila tutti i campi')"></textarea>
                                            </div>

                                            <span>Compilando questo modulo dichiari di essere d'accordo al trattamento dei tuoi dati personali ai sensi della Legge 675/96 e D.Lgs. 196/2003. I dati personali verranno utilizzati esclusivamente per l’espletamento della richiesta. Per ulteriori dettagli leggi la nostra <a href="/privacy-policy">Privacy Policy</a>.</span> 
                                          
                                      <button type="submit" id="submit" name="submit" class="btn btn-primary bottone-form-contatti">Invia!</button>
                                    </form>
                                </div>
                            </div>  
                            <div class="col-md-3"></div>      
            @endif 

                        </div>

@endsection
