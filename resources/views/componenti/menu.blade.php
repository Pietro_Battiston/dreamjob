<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><img src="/img/sito/logo/dreamjob-logo.jpg" alt="logo dream job" class="img-responsive"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/">HOME</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CORSI</a>
          <ul class="dropdown-menu">
              @foreach ($categories as $category )
                  <li><a href="/corsi/{{$category->slug}}">{{$category->title}}</a></li>
               @endforeach
          
          </ul>
        </li>
        <li><a href="/chi-siamo">CHI SIAMO</a></li>
        <li><a href="/contatti">CONTATTI</a></li>

      </ul>
      <form class="navbar-form navbar-left">
        
      </form>
      <ul class="nav navbar-nav navbar-right">
       <li><a href="#" class="contattaci-navmenu" data-toggle="modal" data-target="#largeModal">CONTATTACI SUBITO</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>