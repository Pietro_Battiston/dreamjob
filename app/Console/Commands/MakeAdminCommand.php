<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\User;

class MakeAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $input['email'] = $this->ask('Email?');
       $user = User::where('email', $input)->first();
       $user->is_admin = 1;
       $user->save();

    }
}
