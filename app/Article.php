<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
		protected $fillable = ['title','text', 'img', 'alt_img', 'keyword', 'metatitle', 'metadesc'];



		public function setTitleAttribute($value)
	  	{
		    $this->attributes['title'] = $value;
		    $this->attributes['slug'] = str_slug($value);
	  	}

	  	public function category_relationship()
		{
			return $this->belongsTo('Category');
		}

}
