<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use Tinify;
use App\Article;

class CategoriesController extends Controller
{
    private $imgPath = "/img/upload/";
    private $imgWidth = 640;
    private $imgHeight = 200;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index()
    {
        // $categories = DB::table('categories')->orderBy('id','DESC')->take(6)->get();
        $categories = DB::table('categories')->orderBy('id')->get();
    
        $corsi = Db::table('articles')->inRandomOrder()->take(10)->get();

        


        return view('index3')->with('categories',$categories)->with('corsi',$corsi);

    }
    

    public function category($slug)
    {
       // $notizia = News::findOrFail($id); //Prendo il post con id=$id
        $category = Category::whereSlug($slug)->firstOrFail();
        $lista = Category::with('article_relationship')->get();
        $corsi_categ = $category->article_relationship;
        // if (\Route::current()->getName() == 'categoria-in-aula') {
        //         $corsi_categ = $tutticorsi->where('aula_o_online', 1);
        // }else{
        //         $corsi_categ = $tutticorsi->where('aula_o_online', 2);
        // }
        // $category = Category::whereSlug($slug)->firstOrFail();
        return view('categoria')->with('category', $category)->with('corsi_categ', $corsi_categ); //ritorno la view con il post
    }
   


    public function adminindex()
    {
        // $newsadmin = DB::table('articles')->get();
        return view('admin.index');
        //->with('newsadmin',$newsadmin)
    }

      public function admincategorie()
    {
        //con withCount si può fare un conteggio degli articoli in relazione con le categ e, nella view,
        //all'interno del foreach si usa aggiungendo _count {{$categoria->article_relationship_count}}

        $categorie = Category::withCount('article_relationship')->get();

        return view('admin.categorie')->with('categorie',$categorie);

    }

    private function UploadImage(Request $request)
    {
            if ($request->hasFile('img')) {
                    $destinationPath = './img/';
                    $image = $request->file('img');
                    $fileName = $image->getClientOriginalName();
                    $randomNumber = rand(11111,99999);
                    $newFileName = $randomNumber . "_" . $fileName;
                    //$image->move($destinationPath, $newFileName); 
                    $tocompress = Tinify::fromFile($image);
                    $tocompress->toFile($newFileName);
                    $movefile = rename($newFileName, $destinationPath . $newFileName);
                return $newFileName;
            }
    }

    public function add()
    {
        return view('admin.add', ['categories' => new Category()]);
    }

    public function save(Request $request)
    {
    $categoria = new Category();
            $categoria->title       = $request->input('title');
            $categoria->text        = $request->input('text');
            $categoria->img         = $this->uploadImage($request);
            $categoria->alt_img     = $request->input('alt_img');
            $categoria->metatitle   = $request->input('metatitle');
            $categoria->metadesc    = $request->input('metadesc');

            $categoria->save();
        return redirect('admin/categorie');
                //->with('success', 'admin.notizia.success-insert');

    }

     public function edit($id)
    {
        $category= Category::find($id);
        return view('admin.edit_categ',compact('category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'details' => 'required',
        // ]);
        Category::find($id)->update($request->all());
        return redirect('admin/categorie');
                        //->with('success','Product updated successfully');
    }

     public function destroy($id)
    {
       Category::destroy($id);
        return redirect('admin/categorie');
        
                        //->with('success','Product updated successfully');
    }
}
