<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;
use App\Category;

use Tinify;


class ArticlesController extends Controller
{
    private $imgPath = "/img/upload/";
    private $imgWidth = 640;
    private $imgHeight = 200;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index()
    {
        $news = DB::table('articles')->orderBy('id','DESC')->take(6)->get();
        return view('index2')->with('news',$news);
    }

     public function admincorsi()
    {
        $corsi = DB::table('articles')->orderBy('id','DESC')->get();
        // $article_categ = Article::select('category_id')->firstOrFail();
        // $article_categ = Article::select('category_id')->firstOrFail();
        // $categoria = DB::table('categories')->where('id', '=', $article_categ)->select('title')->get();
        $categorie = DB::table('categories')->orderBy('id','DESC')->get();

        return view('admin.corsi')->with('corsi',$corsi)->with('categorie',$categorie);

    }

    public function article($slug)
    {
       // $notizia = News::findOrFail($id); //Prendo il post con id=$id
        // $notizia = Article::findOrFail($id);
        // $categoriaid = $notizia->category_id;
        $article = Article::whereSlug($slug)->firstOrFail();
        // $categoriaid = $article->category_id;
        // $categoria = DB::table('categories')->where('id', '=', $categoriaid)->select('title')->get();



    return view('article')->with('article',$article); //ritorno la view con il post
    }


    public function adminindex()
    {
        // $newsadmin = DB::table('articles')->get();
        $frasi_motivazionali = array(
            "Sforzati di non avere solo successo, ma piuttosto di essere di valore. <br> -Albert Einstein",
            "Due strade divergevano nel bosco, ed io… io scelsi quella meno battuta e questo fece la differenza. <br> -Robert Frost",
            "La domanda comune che viene chiesto nel business è, ‘perché?’ Questa è una buona domanda, ma una questione altrettanto valida è: ‘perché no?’ <br> -Jeffrey Bezos",
            "Ho sbagliato più di 9000 tiri nella mia carriera. Ho perso quasi 300 partite. 26 volte, mi hanno dato la fiducia per fare il tiro vincente dell’ultimo secondo e ho sbagliato. Ho fallito più e più e più volte nella mia vita. È per questo che ho avuto successo. <br> -Michael Jordan",
            "Gli sciocchi aspettano il giorno fortunato, ma ogni giorno è fortunato per chi sa darsi da fare. <br> -Buddha",
            "Non si può mai attraversare l’oceano se non si ha il coraggio di perdere di vista la riva. <br> -Cristoforo Colombo",
            "Possiamo avere di più di quello che abbiamo perchè possiamo diventare di più di quello che siamo. <br> -Jim Rohn",
            "Qualunque cosa tu possa fare, qualunque sogno tu possa sognare, comincia. L’audacia reca in se genialità, magia e forza. Comincia ora. <br> -Johann Wolfgang von Goethe",
            "Se una voce dentro di te continua a ripeterti “non sarai mai in grado di dipingere”, allora dedicati alla pittura con tutto te stesso, e vedrai che quella voce sarà messa a tacere. <br> -Vincent Van Gogh",
            "C’è solo un modo per evitare le critiche: non fare nulla, non dire nulla, e non essere niente. <br> -Aristotele",
            "Gli ostacoli sono quelle cose spaventose che vedi quando togli gli occhi dalla meta. <br> -Henry Ford",
            "L’unica persona che sei destinato a diventare è la persona che decidi di essere. <br> -Ralph Waldo Emerson",
            "Ogni volta che ti viene chiesto se puoi fare un lavoro, rispondi, “Certo che posso!” Poi datti da fare e scopri come farlo. <br> -Theodore Roosevelt",
            "Tutto quello che hai sempre voluto è dall’altro lato della paura. <br> -George Addair",
            "Inizia dove ti trovi. Usa ciò che hai. Fai ciò che puoi. <br> -Arthur Ashe"

            );
            $frase_random =  array_random($frasi_motivazionali);
        return view('admin.index')->with('frase_random',$frase_random);
        //->with('newsadmin',$newsadmin)
    }


    private function UploadImage(Request $request)
    {
            if ($request->hasFile('img')) {
                    $destinationPath = './img/';
                    $image = $request->file('img');
                    $fileName = $image->getClientOriginalName();
                    $randomNumber = rand(11111,99999);
                    $newFileName = $randomNumber . "_" . $fileName;
                    //$image->move($destinationPath, $newFileName); 
                    $tocompress = Tinify::fromFile($image);
                    $tocompress->toFile($newFileName);
                    $movefile = rename($newFileName, $destinationPath . $newFileName);
                return $newFileName;
            }
    }

    public function add()
    {   
        $news = DB::table('articles')->orderBy('id','DESC')->get();
        $categories = DB::table('categories')->orderBy('id','DESC')->get();
        return view('admin.add_article', ['notizia' => new Article()])->with('categories',$categories);;
    }

    public function save(Request $request)
    {
    $notizia = new Article();
            $notizia->title       = $request->input('title');
            $notizia->text        = $request->input('text');
            $notizia->img         = $this->uploadImage($request);
            $notizia->alt_img     = $request->input('alt_img');
            $notizia->metatitle   = $request->input('metatitle');
            $notizia->metadesc    = $request->input('metadesc');
            $notizia->category_id    = $request->input('category');
            $notizia->aula_o_online    = $request->input('tipologia');


            $notizia->save();
        return redirect('admin/corsi');
                //->with('success', 'admin.notizia.success-insert');

    }

     public function edit($id)
    {
        $article= Article::find($id);
        return view('admin.edit',compact('article'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'details' => 'required',
        // ]);
        Article::find($id)->update($request->all());
        return redirect('admin/corsi');
                        //->with('success','Product updated successfully');
    }

     public function destroy($id)
    {
       Article::destroy($id);
        return redirect('admin/corsi');
                        //->with('success','Product updated successfully');
    }
}
