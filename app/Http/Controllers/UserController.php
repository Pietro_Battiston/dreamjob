<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class UserController extends Controller
{
	    public function index() {
	    	$users = DB::table('users')->get();
        	return view('admin.users')->with('users',$users);
	    }

		    /**
	     * Update the specified resource in storage.
	     *
	     * @param  \Illuminate\Http\Request  $request
	     * @param  int  $id
	     * @return \Illuminate\Http\Response
	     */

	    public function makeadmin($id, Request $request) {
	    	$user = User::FindOrFail($id);
	    	$user->is_admin = 1;
	    	$user->save();
        	
        	return back();

	    }
}
