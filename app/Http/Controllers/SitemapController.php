<?php

namespace App\Http\Controllers;

use Sitemap;

class SitemapController extends Controller
{
    public function index()
    {
        // Get a general sitemap.
        Sitemap::addSitemap('/sitemaps/general');

        // You can use the route helpers too.
        // Sitemap::addSitemap(route('sitemaps.posts'));

        // Return the sitemap to the client.
        return Sitemap::index();
    }
}
