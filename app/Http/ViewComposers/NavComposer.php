<?php

namespace App\Http\ViewComposers;

use App\Category;

class NavComposer
{
    public function compose($view)
    {
        $view->with('menu', Category::all());
    }
}