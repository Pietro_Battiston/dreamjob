<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
		protected $fillable = ['title','text', 'img', 'alt_img', 'keyword', 'metatitle', 'metadesc'];



		public function setTitleAttribute($value)
	  	{
		    $this->attributes['title'] = $value;
		    $this->attributes['slug'] = str_slug($value);
	  	}

	  	public function article_relationship()
    	{
    		return $this->hasMany(Article::class,'category_id');
    	}

}
